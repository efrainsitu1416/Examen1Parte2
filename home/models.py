from django.db import models

# Create your models here.
class Jugador(models.Model):
	nombre_jugador = models.CharField(max_length=50)
	posicion = models.CharField(max_length=32)
	num_jugador = models.IntegerField()
	equipo = models.CharField(max_length=50)
	status = models.CharField(max_length=32)

	def __str__(self):
		return self.nombre_de_jugador


class Equipo(models.Model):
	nombre_equipo = models.CharField(max_length=32)
	num_jugadores = models.IntegerField()
	sede = models.CharField(max_length=32)


	def __str__(self):
		return self.nombre_del_equipo

class Estadio(models.Model):
	nombre = models.CharField(max_length=32)
	direccion = models.CharField(max_length=50)
	equipo_local = models.CharField(max_length=50)


	def __str__(self):
		return self.nombre
