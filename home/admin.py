from django.contrib import admin

# Register your models here.
from .models import Jugadores

admin.site.register(Jugadores)

from .models import Equipos

admin.site.register(Equipos)

from .models import Estadios

admin.site.register(Estadios)
